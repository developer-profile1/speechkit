# -*- coding: utf-8 -*-

import requests
import time
import json

# This example is from Yandex Cloud Docs https://cloud.yandex.ru/docs/speechkit/stt/api/transcribation-ogg
#
# Укажите ваш API-ключ и ссылку на аудиофайл в Object Storage.
key = input("Enter token: ")
filelink = input("Enter URL to file(ogg) in object storage for speechkit: ")
folderid = input("Folder id: ")

POST = "https://transcribe.api.cloud.yandex.net/speech/stt/v2/longRunningRecognize"

body ={
    "config": {
        "specification": {
            "languageCode": "ru-RU",
            "folderId": folderid
        }
    },
    "audio": {
        "uri": filelink
    }
}

header = {'Authorization': 'Bearer {}'.format(key)}

# Отправить запрос на распознавание.
req = requests.post(POST, headers=header, json=body)
data = req.json()
print(data)

id = data['id']

# Запрашивать на сервере статус операции, пока распознавание не будет завершено.
while True:

    time.sleep(1)

    GET = "https://operation.api.cloud.yandex.net/operations/{id}"
    req = requests.get(GET.format(id=id), headers=header)
    req = req.json()

    if req['done']: break
    print("Not ready")

# Показать полный ответ сервера в формате JSON.
print("Response:")
print(json.dumps(req, ensure_ascii=False, indent=2))

# Показать только текст из результатов распознавания.
print("Text chunks:")
for chunk in req['response']['chunks']:
    print(chunk['alternatives'][0]['text'])
